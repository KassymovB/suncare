<div class="relative flex-shrink-0 w-5/12 p-16 pr-24 bg-violet-500">

    <div class="">
        <img src="/img/logo_white.png" alt="Logo">
    </div>

    <div class="mt-16 w-96 overflow-hidden">

        <div class="flex">

            <div class="flex-shrink-0 w-full mr-24">

                <h1 class="text-3xl text-white font-semibold">
                    Интернет маркетинг бойынша жеке көмекші
                </h1>

                <p class="mt-4 text-white">
                    Ең керек материалды ұсыну үшін және нәтижені бақылап отыру үшін интерактивті чат-бот технологиясын
                    қолданамын.
                </p>
            </div>
            <div class="flex-shrink-0 w-96 mr-24">

                <h1 class="text-3xl text-white font-semibold">
                    Интернет маркетинг бойынша жеке көмекші
                </h1>

                <p class="mt-4 text-white">
                    Ең керек материалды ұсыну үшін және нәтижені бақылап отыру үшін интерактивті чат-бот технологиясын
                    қолданамын.
                </p>
            </div>
            <div class="flex-shrink-0 w-96 mr-24">

                <h1 class="text-3xl text-white font-semibold">
                    Интернет маркетинг бойынша жеке көмекші
                </h1>

                <p class="mt-4 text-white">
                    Ең керек материалды ұсыну үшін және нәтижені бақылап отыру үшін интерактивті чат-бот технологиясын
                    қолданамын.
                </p>
            </div>
        </div>
    </div>

    <div class="flex items-center mt-10 ">
        <button class="focus:outline-none z-20">
            <svg class="w-4 h-4 fill-current text-white">
                <use href="#arrow-left"></use>
            </svg>
        </button>

        <div class="flex ml-10 z-20">
            <div class="h-2 w-2 rounded-full bg-white"></div>
            <div class="ml-3 h-2 w-2 rounded-full bg-white opacity-50"></div>
            <div class="ml-3 h-2 w-2 rounded-full bg-white opacity-50"></div>
        </div>

        <button class=" ml-10 focus:outline-none z-20">
            <svg class="w-4 h-4 fill-current text-white transform rotate-180">
                <use href="#arrow-left"></use>
            </svg>
        </button>
    </div>

    <div class="absolute bottom-0 left-0 -right-7 overflow-hidden z-10">
        <img src="../img/login-img.png" alt="Login img" class="w-full">
    </div>
</div>
