<div class="flex flex-col w-full">

    <div class="flex-grow flex flex-col justify-end overflow-y-hidden">

        <div class="px-20 pb-14 w-full overflow-y-auto">


            <div class="flex mt-3 pr-96">

                <div class="flex-shrink-0 flex items-center justify-center w-7 h-7 bg-gray-100 rounded-full">
                    <img src="/img/bot-icon.svg" alt="#" class="w-5 h-5">
                </div>

                <div class="flex flex-col">

                  <?php include "gif.php" ?>

                </div>

            </div>

            <div class="flex mt-3 pr-96">

                <div class="flex-shrink-0 flex items-center justify-center w-7 h-7 bg-gray-100 rounded-full">
                    <img src="/img/bot-icon.svg" alt="#" class="w-5 h-5">
                </div>

                <div class="flex flex-col">

                  <?php include "dialog.php" ?>

                </div>

            </div>

            <div class="flex justify-end pl-96 mt-3 w-full">

                <div class="sf-pro px-6 py-4 bg-blue-600 rounded-3xl text-white">
                    Тамаша
                </div>

            </div>


            <div
                    x-show="openChat"
                    x-transition:enter="transition-all ease-liner duration-1000"
                    x-transition:enter-start="opacity-0 transform translate-y-16"
                    x-transition:enter-end="opacity-1 transform translate-y-0"
                    x-transition:leave="transition-all ease-liner duration-700"
                    x-transition:leave-start="opacity-1 transform translate-y-0"
                    x-transition:leave-end="opacity-0 transform translate-y-16"
                    class="flex justify-end pl-96 mt-3 w-full"
                    class="flex justify-end pl-96 mt-3 w-full"
            >

                <div class="sf-pro px-6 py-4 bg-blue-600 rounded-3xl text-white">

                    Жоқ, қазір ашайық


                </div>

            </div>

            <div
                    x-show="openChat2"
                    x-transition:enter="transition-all ease-liner duration-1000"
                    x-transition:enter-start="opacity-0 transform translate-y-16"
                    x-transition:enter-end="opacity-1 transform translate-y-0"
                    x-transition:leave="transition-all ease-liner duration-700"
                    x-transition:leave-start="opacity-1 transform translate-y-0"
                    x-transition:leave-end="opacity-0 transform translate-y-16"
                    class="flex justify-end pl-96 mt-3 w-full"
            >

                <div class="sf-pro px-6 py-4 bg-blue-600 rounded-3xl text-white">

                    Бар


                </div>

            </div>

        </div>
    </div>


    <div class="flex items-center justify-center flex-shrink-0 h-20 border-t-2 border-gray-200 bg-gray-100">

         <?php include "./btn-input/questions-btn.php"; ?>

    </div>
</div>
