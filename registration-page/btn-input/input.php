<form class="flex items-center w-full px-20">

    <div class="relative w-full">
        <input type="tel"
               name="name"
               placeholder="+7 _ _ _   _ _ _   _ _ _ _"
               required
               class="px-4 py-2 w-full border border-gray-300 focus:outline-none focus:border-blue-600 rounded-xl">

        <span class="absolute top-2/4 right-4 transform -translate-y-2/4 nunito text-lg text-gray-300">30</span>
    </div>

    <button class="ml-12 text-gray-500 focus:text-blue-600 focus:outline-none">
        <svg class="w-6 h-6 fill-current">
            <use href="#sent"></use>
        </svg>
    </button>

</form>

