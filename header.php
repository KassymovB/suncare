<header class="flex items-center justify-between px-8 pr-16 py-6">

    <a href="#">
        <img src="/img/logo.png" alt="Logo">
    </a>

    <div class="flex items-center">

        <a href="#" class="relative">

            <svg class="w-6 h-6 fill-current text-gray-200 hover:text-gray-300">
                <use href="#notification"></use>
            </svg>


            <span class="absolute -top-3 -right-3 py-0.5 px-2 bg-gray-400 border-2 border-white rounded-full text-white text-xs font-bold">
                            1
                    </span>
        </a>

        <a href="#" class="ml-14">

            <div class="flex items-center justify-center w-8 h-8 rounded-full bg-gray-200 hover:bg-gray-300 group">

                <svg class="w-6 h-6 fill-current text-gray-500 group-hover:text-gray-600">
                    <use href="#user"></use>
                </svg>

            </div>

        </a>

        <button class="ml-14 focus:outline-none">

            <svg class="w-6 h-6 fill-current text-gray-200 hover:text-gray-300">
                <use href="#category"></use>
            </svg>
        </button>

    </div>

</header>
