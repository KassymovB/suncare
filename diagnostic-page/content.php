<div class="flex flex-col mx-auto max-w-3xl h-full">

    <div class="flex flex-col items-center py-4 px-8 rounded-3xl bg-gradient-to-r from-violet-600 to-violet-400">

        <h1 class="text-white text-2xl font-medium">Диагностика</h1>


        <div class="mt-10 text-white font-semibold">
            1 / 10 сұрақ
        </div>

        <div class="mt-2 w-3/5 h-1 bg-violet-400 rounded-3xl">
            <div class="w-1/5 h-1 bg-white rounded-3xl">
            </div>
        </div>

        <p class="mt-8 text-white text-sm">
            Төмендегі тұжырымдар сені қаншалықты сипаттайды?
        </p>

    </div>

    <div class="flex flex-col justify-between pb-4 mt-10 h-full overflow-y-hidden">

        <div class="flex-grow">

            <?php include 'question-active.php' ?>

            <?php include 'question-disabled.php' ?>

        </div>

        <div class="pt-8 w-full border-t border-gray-200 ea">
            <button class="block mx-auto px-24 py-3 bg-violet-600 rounded-2xl
             text-white font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-violet-500
              hover:shadow-2xl focus:translate-y-0">
                Жіберу
            </button>
        </div>


    </div>


</div>