<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="public/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">

    <style>
        [x-cloak] {
            display: none;
        }
    </style>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <title>Document</title>
</head>


<body class="antialiased">
<div style="display: none">
    <?php include "../src/icons.svg"; ?>
</div>

    <div class="relative h-screen overflow-y-hidden">

        <?php include "diagnostic-header.php"; ?>

        <section class="relative pt-20 mx-auto max-w-screen-xl h-full">

            <?php require_once "content.php"; ?>

        </section>

    </div>
</body>

</html>