<div class="py-14 border-t border-gray-200 text-center">

    <div class="text-2xl font-medium">
        Көбіне назарым ескі идея мен жобадан жаңа идея мен жобаға ауып кетеді
    </div>

    <div class="flex items-center justify-between mt-10">

        <div class="text-sm font-semibold text-red-500">
            Маған мүлдем ұқсамайды
        </div>

        <div class="relative">
            <input id="1" type="radio" name="132" class="custom-radio hidden">
            <label for="1" class="w-12 h-12"></label>
        </div>

        <div class="relative">
            <input id="2" type="radio" name="132" class="custom-radio hidden">
            <label for="2" class="w-10 h-10"></label>
        </div>

        <div class="relative">
            <input id="3" type="radio" name="132" class="custom-radio-gray hidden">
            <label for="3" class="w-7 h-7"></label>
        </div>

        <div class="relative">
            <input id="4" type="radio" name="132" class="custom-radio-green hidden">
            <label for="4" class="w-10 h-10"></label>
        </div>

        <div class="relative">
            <input id="5" type="radio" name="132" class="custom-radio-green hidden">
            <label for="5" class="w-12 h-12"></label>
        </div>

        <div class="text-sm font-semibold text-emerald-500">
            Маған қатты ұқсайды
        </div>
    </div>

</div>