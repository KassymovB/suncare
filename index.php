<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="public/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">

    <style>
        [x-cloak] {
            display: none;
        }
    </style>

    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <title>Document</title>
</head>

<div style="display: none">
    <?php include "./src/icons.svg"; ?>
</div>

<body
        x-data="{openList: false, openText: false}"
      class="antialiased"
>

    <main class="pb-8">

        <?php include "header.php"; ?>

        <section class="relative mt-3 mx-auto max-w-screen-xl">

                <?php require_once "./main-page/index.php"; ?>

        </section>

    </main>

</body>

</html>