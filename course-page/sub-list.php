    <ul
            x-show="openList"
            x-cloak=""
            x-transition:enter="ease-linear duration-500"
            x-transition:enter-start="opacity-0 "
            x-transition:enter-end="opacity-100"
            x-transition:leave="ease-linear duration-300"
            x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            class="mt-5 transition-all transform"
    >

        <?php include 'subitem-completed.php' ?>

        <?php include 'subitem-active.php' ?>

        <?php include 'subitem-disabled.php' ?>

        <?php include 'subitem-disabled.php' ?>
    </ul>