    <button class="mt-8 flex items-center justify-between p-6 bg-gray-100 rounded-2xl border border-gray-300 
    focus:border-blue-500 focus:outline-none group" style="width: 360px;">

        <div class="flex items-center">

            <figure class="w-9 h-9 overflow-hidden">
                <img src="/img/bot.png" alt="bot" class="w-full h-full">
            </figure>

            <div class="ml-3">
                <div class="text-xs font-bold text-gray-400">Куратор - Айжан Беспаева</div>
                <div class="text-sm">“Көмек керек пе?”</div>
            </div>

        </div>

        <svg class="w-7 h-7 fill-current text-gray-200 group-focus:text-blue-600">
            <use href="#chat"></use>
        </svg>
    </button>