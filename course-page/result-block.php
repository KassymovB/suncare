 <!-- 
           mobile-lesson:
           bg: from-lightblue-50 to-lightblue-200
           
           progressbar:
           1-bg: bg-indigo-200
           2-bg-lightblue-500
           img:"/img/heart-2.png"
           -----

           targeting-lesson:
           bg: from-pink-100 to-purple-200

           progressbar:
           1-bg-violet-200
           2-bg-violet-400
            img:"/img/targ-2.png"
           -----

           copyraiting-lesson:
           bg: from-emerald-50 to-emerald-200

           progressbar:
            1-bg-green-200
            2-bg-green-700 bg-opacity-50
            img:"/img/copy-2.png"
           -----
        -->

 <div class="p-10 pb-20 bg-gradient-to-r from-orange-100 to-rose-100 rounded-3xl">

     <div class="flex flex-col items-center">

         <div class="w-32 h-36 overflow-hidden">
             <img src="/img/puzzle-sidebar.png" alt="puzzle">
         </div>

         <div class="mt-10 text-lg font-bold">
             Web дизайн
         </div>

         <div class="text-xs text-gray-500 font-bold">
             Курс
         </div>

     </div>

     <div class="mt-16">

         <div class="flex items-center justify-between">
             <span class="inline-block text-sm font-bold">cабақ</span>
             <span class="inline-block text-sm font-bold">2/10</span>
         </div>

         <div class="mt-2 h-1 bg-rose-200 rounded-3xl">
             <div class="w-1/5 h-1 bg-rose-300 rounded-3xl">
             </div>
         </div>

         <div class="mt-10">
             <div class="flex items-center justify-between">
                 <span class="inline-block text-sm font-bold">cабақ</span>
                 <span class="inline-block text-sm font-bold">2/10</span>
             </div>

             <div class="mt-2 h-1 bg-rose-200 rounded-3xl">
                 <div class="w-1/5 h-1 bg-rose-300 rounded-3xl">
                 </div>
             </div>

             <div class="flex items-center justify-between mt-10">
                 <span class="inline-block text-sm font-bold">пікірлер</span>
                 <span class="inline-block text-sm font-bold">188</span>
             </div>

         </div>

     </div>

 </div>