  <div class="relative mt-9 w-full rounded-3xl">

      <div class="absolute top-1/2 left-8 py-1 transform -translate-y-1/2 z-30">

          <svg class="w-6 h-6 fill-current text-gray-400 ">
              <use href="#diplom"></use>
          </svg>

      </div>

      <div class="relative pl-24 pr-36 pr-6 py-16 rounded-3xl bg-gradient-to-r from-violet-100 to-violet-300 overflow-hidden z-20 group">


          <div class="text-2xl font-bold">
              Интернет - маркетолог атану
          </div>

          <div class="text-sm"> Диплом алу</div>


          <div class="absolute bottom-0 right-0 transform w-36 overflow-hidden  transition-all ease-linear duration-150 transform
                                 group-hover:scale-105 z-30">
              <img src="/img/flag.png" alt="#" class="w-full">

          </div>

          <div class="absolute top-32 right-20 w-96 h-96 bg-violet-200 rounded-3xl transform rotate-45 opacity-50" style="z-index: -1;">

          </div>

          <div class="absolute top-24 -right-32 w-96 h-96 bg-violet-200 rounded-3xl transform rotate-45 opacity-50" style="z-index: -1;">

          </div>
      </div>


  </div>