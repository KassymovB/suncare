<div class="flex justify-between">

    <div class="w-3/5">

        <div class="ml-14">

            <h1 class="text-gray-400 text-xl font-bold">Cәлем,</h1>

            <div
                    @click="openText = !openText"
                    class="flex items-center text-3xl font-bold cursor-pointer">
                Қуаныш!
                <img src="/img/welcome.svg" alt="welcome" class="ml-2">
            </div>

            <div
                    x-show="openText"
                    x-transition:enter="transition-all ease-liner duration-700"
                    x-transition:enter-start="opacity-0 transform scale-0"
                    x-transition:enter-end="opacity-1 transform scale-100"
                    x-transition:leave="transition-all ease-liner duration-700"
                    x-transition:leave-start="opacity-1 transform scale-100"
                    x-transition:leave-end="opacity-0 transform scale-0"
                    class="flex items-center mt-8"
            >

                <div class="w-16 overflow-hidden">
                    <img src="/img/kuanysh.png" alt="#" class="w-full">
                </div>

                <div class="ml-8 p-4 w-7/12 bg-gray-100 text-lg font-semibold rounded-2xl rounded-bl-none">
                        Диагностика өтсең ерінбей,Мотивация болады тіленбей
                </div>

            </div>

        </div>

        <?php include "diagnostika.php" ?>

        <?php include "diagnostika-completed.php" ?>

        <?php include "lock-block.php" ?>

    </div>

    <?php include "main-sidebar.php" ?>

</div>