  <div class="relative mt-9 px-9 py-10 pr-40 rounded-3xl bg-gradient-to-br from-violet-600 to-violet-300 group">

      <div class="text-lg font-bold text-violet-400">
          Диагностика
      </div>

      <p class="mt-3 text-2xl text-white font-bold">
          Қанша теңге таба алатыныңды білгің келе ме?
      </p>

      <div class="mt-8">

          <a href="#" class="relative inline-flex items-center px-5 py-1 bg-white rounded font-bold text-blue-600">
              Кеттік

              <svg class="ml-3 w-3 h-3 fill-current hover:text-white z-50">
                  <use href="#chevron-right"></use>
              </svg>
          </a>
      </div>

      <div class="absolute top-2/4 right-9 transform -translate-y-2/4 w-28 overflow-hidden transition-all ease-liner duration-150 transform group-hover:scale-105">
          <img src="/img/diagnostic.png" alt="#" class="w-full">
      </div>

  </div>