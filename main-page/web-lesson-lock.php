    <div class="relative pr-36 px-9 py-16 mt-9 w-10/12 rounded-3xl bg-gradient-to-r from-orange-50 to-rose-200 group">
        <div class="absolute top-1/2 -left-7 py-1 transform -translate-y-1/2 bg-white z-10">

            <svg class="w-4 h-4 fill-current text-gray-300 z-10">
                <use href="#lock"></use>
            </svg>

        </div>


        <div class="text-2xl font-bold">
            Web дизайн
        </div>

        <p class="mt-2 text-lg">
            Сататын сайт құрастыру
        </p>

        <p class="mt-2 text-lg">
           11 сабақ
        </p>

        <div
                class="absolute top-2/4 right-0 transform -translate-y-2/4 w-28
                       overflow-hidden transition-all ease-linear duration-150 transform group-hover:scale-105"
        >

            <img src="/img/puzzle.png" alt="#" class="w-full">
        </div>
    </div>