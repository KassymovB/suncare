

    <div class="relative mt-9  pr-32 pl-24 px-9 py-5 w-full rounded-3xl bg-gradient-to-r from-orange-50 to-rose-200 group">
        <div class="absolute top-1/2 left-8 py-4 transform -translate-y-1/2 z-10">

            <svg class="w-6 h-6 fill-current text-gray-300">
                <use href="#circle"></use>
            </svg>
        </div>

        <div class="text-2xl font-bold">
            Web дизайн
        </div>

        <p class="text-lg">
            Сататын сайт құрастыру
        </p>

        <div class="mt-4 w-2/4">

            <div class="h-1 bg-rose-200 rounded-3xl">
                <div class="w-full h-1 bg-rose-300 rounded-3xl"></div>
            </div>

            <div class="flex items-center justify-between mt-2">

                <div class="flex items-center text-rose-300">
                    <span class="inline-block text-sm font-bold">10/10 сабақ</span>

                    <svg class="ml-3 w-3 h-3 fill-current">
                        <use href="#completed"></use>
                    </svg>
                </div>

                <div class="flex items-center text-rose-300">
                    <span class="inline-block text-rose-300 text-sm font-bold">10/10 тапсырма</span>

                    <svg class="ml-3 w-3 h-3 fill-current">
                        <use href="#completed"></use>
                    </svg>
                </div>

            </div>

        </div>

        <div class="mt-8">

            <a href="#" class="inline-flex items-center px-5 py-1 bg-white rounded font-bold text-blue-600">
                Ашу

                <svg class="ml-3 w-3 h-3 fill-current">
                    <use href="#chevron-right"></use>
                </svg>
            </a>
        </div>



        <div
                class="absolute top-2/4 right-0 transform -translate-y-2/4 w-28
                       overflow-hidden transition-all ease-linear duration-150 transform group-hover:scale-105"
        >

            <img src="/img/puzzle.png" alt="#" class="w-full">
        </div>

    </div>



