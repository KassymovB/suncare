  <div class="relative mt-9 mx-auto px-9 pr-40 py-10 w-10/12 rounded-3xl bg-gradient-to-br from-violet-600 to-violet-300 group">

      <div class="text-lg font-bold text-violet-400">
          Диагностика
      </div>

      <p class="mt-3 text-2xl text-white font-bold">
          Cенің болжамдық
          айлық табысың 430 000 ₸
      </p>

      <div class="mt-8">
          <a href="#" class="relative inline-flex items-center px-5 py-2 bg-white bg-opacity-20 rounded font-bold text-white">
              Қайта өту

              <svg class="ml-3 w-3 h-3 fill-current">
                  <use href="#chevron-right"></use>
              </svg>
          </a>

      </div>


      <div class="absolute top-2/4 right-9 transform -translate-y-2/4 w-28 overflow-hidden transition-all ease-liner duration-150 transform group-hover:scale-105">
          <img src="/img/diagnostic.png" alt="#" class="w-full">
      </div>

  </div>