       <!-- 
           mobile-lesson:
           bg: from-lightblue-50 to-lightblue-200
           img:"/img/heart.png"
           -----

           targeting-lesson:
           bg: from-pink-100 to-purple-200
           img: "/img/targ.png"
           -----

           copyraiting-lesson:
           bg: from-emerald-50 to-emerald-200
           img: "/img/copy.png
           -----

 -->
       <div class="relative pr-36 px-9 py-5 mt-9 w-10/12 rounded-3xl bg-gradient-to-r from-orange-50 to-rose-200 group">

           <svg class="absolute top-1/2 -left-8 transform -translate-y-1/2 w-6 h-6 fill-current text-gray-300">
               <use href="#circle"></use>
           </svg>


           <div class="text-2xl font-bold">
               Web дизайн
           </div>

           <p class="text-lg">
               Сататын сайт құрастыру
           </p>

           <div class="flex space-x-7 mt-6">

               <span class="text-lg">0/10 сабақ</span>

               <span class="text-lg">0/10 тапсырма</span>

           </div>

           <div class="mt-8">

               <a href="#" class="inline-flex items-center px-5 py-1 bg-white rounded font-bold text-blue-600">
                   Ашу

                   <svg class="ml-3 w-3 h-3 fill-current">
                       <use href="#chevron-right"></use>
                   </svg>
               </a>
           </div>


           <div
                   class="absolute top-2/4 right-0 transform -translate-y-2/4 w-28
                       overflow-hidden transition-all ease-linear duration-150 transform group-hover:scale-105"
           >

               <img src="/img/puzzle.png" alt="#" class="w-full">
           </div>
       </div>



