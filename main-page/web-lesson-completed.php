    <div class="relative pr-36 px-9 py-5 mt-9 w-10/12 rounded-3xl bg-gradient-to-r from-orange-50 to-rose-200 group">

        <div class="absolute top-1/2 -left-8 py-2 bg-white transform -translate-y-1/2 z-10">

            <svg class="w-5 h-5 fill-current text-gray-300">
                <use href="#completed"></use>
            </svg>
        </div>

        <div class="text-2xl font-bold">
            Web дизайн
        </div>

        <p class="text-lg">
            Сататын сайт құрастыру
        </p>

        <div class=" mt-6">
            <span class="inline-block text-lg">0/10 сабақ</span>

            <span class="inline-block ml-14 text-lg">0/10 тапсырма</span>
        </div>

        <div class="mt-8">

            <a href="#" class="inline-flex items-center px-5 py-1 bg-white rounded font-bold text-blue-600">
                Ашу

                <svg class="ml-3 w-3 h-3 fill-current">
                    <use href="#chevron-right"></use>
                </svg>
            </a>
        </div>


        <div
                class="absolute top-2/4 right-0 transform -translate-y-2/4 w-28
                       overflow-hidden transition-all ease-linear duration-150 transform group-hover:scale-105"
        >

            <img src="/img/puzzle.png" alt="#" class="w-full">
        </div>
    </div>


