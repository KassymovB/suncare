<li class="mt-1">

    <div class="relative flex items-center py-5 px-6 hover:bg-gray-50 opacity-50">

        <div class="flex items-center">

            <svg class="mr-2 w-6 h-6 fill-current text-white">
                <use href="#border"></use>
            </svg>

            <span class="text-lg font-bold cursor-pointer"> Бәсекелестерді талдау </span>
        </div>
    </div>
</li>