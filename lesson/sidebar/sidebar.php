<div
    x-show="openSidebar"
    x-cloak=""
    x-trasnition:enter="transition ease-liner duration-700"
    x-trasnition:leave="transition ease-liner duration-700"
    class="fixed left-14 inset-0 z-10"
>

    <div
        x-show="openSidebar"
        @click.prevent="openSidebar = !openSidebar"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-400"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        class="absolute inset-0 bg-black-light bg-opacity-50 z-20"
    >

    </div>

    <div
        x-show="openSidebar"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-700"
        x-transition:enter-start="opacity-0 transform -translate-x-96"
        x-transition:enter-end="opacity-1 transform translate-x-0"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-1 transform translate-x-0"
        x-transition:leave-end="opacity-0 transform -translate-x-96"

        class="absolute left-0 inset-y-0 py-9 px-7 bg-white overflow-y-auto z-40" style="width: 450px"
    >
        <h1 class="flex items-center text-xl font-semibold">
            <svg class="mr-2 w-3 h-3 fill-current  transform rotate-180">
                <use href="#chevron-right"></use>
            </svg>
            Сататын сайт құрастыру
        </h1>

        <?php include "sidebar-list.php"; ?>


    </div>
</div>
