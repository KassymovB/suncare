<div class="relative flex justify-center h-40 overflow-hidden">

    <div class="relative flex flex-col items-center justify-center pb-16 border-4 border-color-orange rounded-full" style="width: 300px; height: 300px">

        <div class="text-6xl text-orange-300 font-semibold">
            48%
        </div>

        <div class="text-orange-300 font-semibold">
            тест нәтижесі
        </div>

        <div class="absolute bottom-2/4 transform translate-y-2.5 -left-2.5 w-4 h-2 bg-orange-300 rounded-3xl"></div>
        <div class="absolute top-6 right-12 transform -rotate-45 w-4 h-2 bg-gray-300 rounded-3xl"></div>
        <div class="absolute bottom-2/4 transform translate-y-2.5 -right-2.5 w-4 h-2 bg-gray-300 rounded-3xl"></div>
    </div>

</div>

<div class="mt-16 text-center">
    <div class="text-2xl font-semibold">
        Өкінішке қарай тесттен өте алмадың
    </div>

    <div class="text-gray-400 font-semibold">
        Бүгінге 2 мүмкіндік бар
    </div>
</div>