<div class="inline-block relative mx-auto text-center progress">

    <div class="relative overflow-hidden" style="width: 300px; height: 150px">
        <div class="absolute top-0 left-0 bar bar--green">
        </div>
    </div>
    <div class="absolute top-2/4 left-2/4 transform -translate-x-2/4 text-6xl font-semibold text-green-500">
        <span>75</span>%
        <div class="text-lg font-medium">тест нәтижесі</div>
    </div>


    <div class="absolute bottom-1 transform translate-y-2.5 -left-1.5 w-4 h-2 bg-green-500 rounded-3xl"></div>
    <div class="absolute top-10 right-10 transform -rotate-45 w-4 h-2 bg-gray-300 rounded-3xl"></div>
    <div class="absolute bottom-1 transform translate-y-2.5 -right-1.5 w-4 h-2 bg-gray-300 rounded-3xl"></div>

</div>
<div class="mt-16 text-center">
    <div class="text-2xl font-semibold">
        Жарайсың! Тесттен өттің!
    </div>

    <div class="text-gray-400 font-semibold">
        Әрі қарай оқуды жалғастыра бер
    </div>
</div>
