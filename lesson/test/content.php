<div class="flex-grow flex flex-col">

    <h1 class="mt-14 text-4xl font-semibold">Тест “Копирайтинг негіздері”</h1>

    <div class="flex-grow flex flex-col justify-center mt-14">
        <?php include 'test.php';?>
    </div>

</div>
