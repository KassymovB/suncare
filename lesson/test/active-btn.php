<div class="flex items-center justify-between mt-14 w-full">

    <button class="flex items-center text-blue-600 font-semibold focus:outline-none hover:text-blue-500">

        <svg class="mr-2 w-3 h-3 fill-current transform rotate-180">
            <use href="#chevron-right"></use>
        </svg>

        Кейінгі
    </button>

    <button
        class="flex items-center px-24 py-3 bg-blue-600 rounded-2xl
             text-white font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:shadow-2xl focus:translate-y-0"
    >
        Келесі

        <svg class="ml-2 w-3 h-3 fill-current">
            <use href="#chevron-right"></use>
        </svg>

    </button>
</div>
