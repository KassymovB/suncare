<div class="flex items-center justify-between mt-14 w-full">

    <button class="flex items-center text-gray-400 font-semibold focus:outline-none hover:text-gray-300">

        <svg class="mr-2 w-3 h-3 fill-current transform rotate-180">
            <use href="#chevron-right"></use>
        </svg>

        Кейінгі
    </button>

    <button
        class="flex items-center px-24 py-3 text-gray-500 bg-gray-300 rounded-2xl
              font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2s hover:shadow-2xl focus:translate-y-0"
    >
        Келесі

        <svg class="ml-2 w-3 h-3 fill-current">
            <use href="#chevron-right"></use>
        </svg>

    </button>
</div>
