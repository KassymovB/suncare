<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="public/style.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;500;600;700&display=swap" rel="stylesheet">

    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600&display=swap" rel="stylesheet">


    <style>
        [x-cloak] {
            display: none;
        }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>
    <title>Document</title>
</head>

<div style="display: none">
    <?php include "../src/icons.svg"; ?>
</div>


<body x-data="{openNps: false, openSidebar: false, openList: false}" class="antialiased">

<button
        x-show="!openSidebar"
        @click="openSidebar = !openSidebar"
        x-cloak=""
        class="fixed left-0 inset-y-0 flex items-center justify-center px-3 border-r border-gray-100 focus:outline-none"
 >

    <svg
            x-transition:enter="transition-all ease-liner duration-700"
            x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-1"
            class="ml-2 w-5 h-5 fill-current"
    >
        <use href="#menu-sidebar"></use>
    </svg>

<!--    <svg-->
<!--            x-show="openSidebar"-->
<!--            x-transition:enter="transition-all ease-liner duration-500"-->
<!--            x-transition:enter-start="opacity-0 transform rotate-180"-->
<!--            x-transition:enter-end="opacity-1 transform rotate-0"-->
<!---->
<!--            class="ml-2 w-5 h-5 fill-current"-->
<!--    >-->
<!--        <use href="#X"></use>-->
<!--    </svg>-->
</button>



<?php include './sidebar-2/sidebar.php';?>

<?php include "nps/nps-question.php"; ?>

<div class="flex mx-auto pb-8 min-h-screen max-w-screen-md">

    <?php require_once "../lesson/content.php"; ?>

</div>

<script src="/src/js/app.js"></script>

</body>
</html>