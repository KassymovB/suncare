<li class="">

    <a href="#" class="flex items-center justify-between py-5 px-6 hover:bg-gray-50">

        <div class="flex items-center">

            <svg class="mr-2 w-6 h-6 fill-current text-green-600">
                <use href="#done"></use>
            </svg>

            <span class="text-lg font-bold cursor-pointer"> Бәсекелестерді талдау </span>
        </div>

    </a>
</li>