<li class="">

    <div
            @click.prevent="openList = !openList"
            class="flex items-center justify-between -mr-7 -ml-12 px-7 py-5 cursor-pointer hover:bg-gray-50">
        <div class="flex text-lg font-bold">

            <div class="mr-2">
                1.
            </div>

            Сайт дегеніміз не?
        </div>

        <div class="flex items-center">

            <span class="text-green-500 text-xs font-bold">1/4</span>


            <span class="ml-8 focus:outline-none">

                <svg x-show="!openList" class="w-4 h-4 fill-current text-gray-200">
                    <use href="#chevron-down"></use>
                </svg>

                <svg x-show="openList" class="w-4 h-4 fill-current text-gray-200">
                    <use href="#chevron-up"></use>
                </svg>

            </span>
        </div>

    </div>

    <?php include 'sidebar-sublist.php'; ?>

</li>