<div
        x-show="openSidebar"
        x-cloak=""
    x-trasnition:enter="transition ease-liner duration-700"
    x-trasnition:leave="transition ease-liner duration-700"
    class="fixed inset-0 z-10"
>

    <div
        x-show="openSidebar"
        @click.prevent="openSidebar = !openSidebar"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-500"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition ease-liner duration-500"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        class="absolute inset-0 bg-black-light bg-opacity-50 z-20"
    >

    </div>

    <div
            x-show="openSidebar"
            x-cloak=""
            x-transition:enter="transition ease-liner duration-1000"
            x-transition:enter-start="sidebar-go"
            x-transition:enter-end="sidebar-come"
            x-transition:leave="transition ease-liner duration-1000"
            x-transition:leave-start="sidebar-come"
            x-transition:leave-end="sidebar-go"

            class="fixed flex items-center h-full z-40"
    >

        <button
                @click="openSidebar = !openSidebar"
                x-cloak=""
                class="flex items-center justify-center px-3 h-full bg-white border-r border-gray-100 focus:outline-none z-40"
        >

            <svg
                    class="ml-2 w-5 h-5 fill-current"
            >
                <use href="#X"></use>
            </svg>

        </button>

        <div

                class="py-9 px-7 pl-12 h-full bg-white overflow-y-auto z-30" style="width: 450px"
        >

            <h1 class="flex items-center text-xl font-semibold">
                <svg class="mr-2 w-3 h-3 fill-current  transform rotate-180">
                    <use href="#chevron-right"></use>
                </svg>
                Сататын сайт құрастыру
            </h1>

            <?php include "sidebar-list.php"; ?>

        </div>


    </div>


</div>
