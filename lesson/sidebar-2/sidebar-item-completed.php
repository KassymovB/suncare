<li class="">

    <div
            @click.prevent="openList = !openList"
            class="flex items-center justify-between -mr-7 -ml-12 px-7 py-5 border-t border-gray-300 cursor-pointer hover:bg-gray-50">

        <div class="flex text-lg font-bold">

            <div class="mr-2">
                2.
            </div>

            Сайт дегеніміз не?
        </div>

        <div class="flex items-center">

            <svg class="w-4 h-4 fill-current text-green-500">
                <use href="#check"></use>
            </svg>


            <span class="ml-8 focus:outline-none">

                <svg x-show="!openList" class="w-4 h-4 fill-current text-gray-200">
                    <use href="#chevron-down"></use>
                </svg>

                <svg x-show="openList" class="w-4 h-4 fill-current text-gray-200">
                    <use href="#chevron-up"></use>
                </svg>

            </span>

        </div>

    </div>

</li>
