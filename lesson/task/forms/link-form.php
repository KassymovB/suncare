    <input
            type="text"
            placeholder="Сілтеме"
            class="py-7 px-6 w-full bg-gray-100 border rounded-2xl font-semibold focus:outline-none focus:border-blue-600"
    >

    <div class="mt-3 pl-7 text-sm text-gray-300 font-semibold">
        Браузерден сайт адресін (сілтемені) көшіріп, осында енгізу керек
    </div>

<div class="flex items-center justify-end text-right mt-14 pt-8 w-full">

    <button class="flex items-center text-gray-400 font-semibold focus:outline-none hover:text-gray-300">

        Пікір қосу

    </button>

    <button
        class="flex items-center ml-16 px-14 py-3 text-white bg-blue-600 rounded-2xl
             text-white font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Тексеріп жіберу

    </button>

</div>
