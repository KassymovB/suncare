
<div class="flex items-center">

    <div class="w-9 h-9 rounded-full bg-gray-200"></div>

    <div class="ml-2">
        <div class="text-sm text-gray-400 font-semibold">
            Омар Алишер
        </div>

        <div class="text-sm text-gray-400">
            10 шілде 20:22
        </div>
    </div>
</div>

<div class="mt-6 font-semibold">
    Cілтеме
    <a href="http://tiktok.tilda.ws/" class="text-blue-600">http://tiktok.tilda.ws/</a>
</div>


<div class="mt-16">

    <div class="flex items-center">

        <div class="w-9 h-9 rounded-full overflow-hidden">
            <img src="../../../img/bot.png" alt="#" class="w-full h-full">
        </div>

        <div class="ml-2">
            <div class="text-sm text-gray-400 font-semibold">
                Омар Алишер
            </div>

            <div class="text-sm text-gray-400">
                10 шілде 20:22
            </div>
        </div>

    </div>

    <div class="mt-6 font-semibold">
        Өкінішке орай қабылданбайды. Cебебі бұл сайт тіркелген бізде
    </div>

</div>

<div class="mt-16">

    <div class="flex items-center">

        <div class="w-9 h-9 rounded-full bg-gray-200"></div>

        <div class="ml-2">
            <div class="text-sm text-gray-400 font-semibold">
                Омар Алишер
            </div>

            <div class="text-sm text-gray-400">
                10 шілде 20:22
            </div>
        </div>
    </div>

    <div class="mt-6 font-semibold">
        Cілтеме <a href="#" class="text-blue-600">http://tiktok.tilda.ws/</a>
    </div>

    <div class="mt-2 font-semibold">
        Кешірім сұраймын. Қате жазып жіберіптім
    </div>
</div>

<div class="flex items-center justify-center text-right mt-10 pt-8 w-full">

    <button
        class="flex items-center ml-8 px-20 py-3 text-white bg-blue-600 rounded-2xl
             text-white font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Келесі

    </button>

</div>
