<div class="relative p-4 bg-gray-100">

    <div class="flex items-center justify-center h-60 border-2 border-dashed border-gray-300">

        <button
            class="flex items-center px-14 py-3 text-white bg-gray-500 rounded-2xl
             text-white text-sm font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-gray-400 hover:shadow-2xl focus:translate-y-0"
        >
            Файл таңдау

        </button>

        <div class="absolute bottom-1 left-2/4 transform -translate-x-2/4 p-1 bg-gray-100 text-sm ">
            Немесе&nbsp;файлды&nbsp;басып&nbsp;тұрып&nbsp;осы&nbsp;жерге&nbsp;лақтырыңыз
        </div>
    </div>


</div>


<input

    x-show="openQuestion"
    type="text"
    placeholder="Пікір"
    class="mt-12 py-7 px-6 w-full bg-gray-100 border rounded-2xl font-semibold focus:outline-none focus:border-blue-600"
>


<div class="flex items-center justify-end mt-14 pt-8 w-full">

    <button
        x-show="!openQuestion"

        @click.prevent="openQuestion = !openQuestion"
        class="flex items-center text-gray-400 font-semibold focus:outline-none hover:text-gray-300"
    >

        Пікір қосу

    </button>

    <button
        class="flex items-center ml-16 px-14 py-3 text-white bg-blue-600 rounded-2xl
             text-white font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Тексеріп жіберу

    </button>

</div>
