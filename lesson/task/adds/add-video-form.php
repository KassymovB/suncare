
    <div class="flex items-center">

        <div class="w-9 h-9 rounded-full bg-gray-200"></div>

        <div class="ml-2">
            <div class="text-sm text-gray-400 font-semibold">
                Омар Алишер
            </div>

            <div class="text-sm text-gray-400">
                10 шілде 20:22
            </div>
        </div>
    </div>

    <div class="mt-6 font-semibold">
        Cілтеме <a href="https://www.youtube.com/watch?v=QtNy-QKkC_4" class="text-blue-600">https://www.youtube.com/watch?v=QtNy-QKkC_4</a>
    </div>

<!--    <div>-->
<!--        -->
<!--    </div>-->


<div class="flex items-center justify-end text-right mt-10 pt-8 w-full">

    <button class="flex items-center px-10 py-3 bg-gray-200 text-gray-500 rounded-2xl font-semibold
            focus:outline-none transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-gray-100 hover:shadow-2xl focus:translate-y-0">

        Жою

    </button>

    <button class="flex items-center ml-8 px-10 py-3 bg-gray-200 text-gray-500 rounded-2xl font-semibold
            focus:outline-none transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-gray-100 hover:shadow-2xl focus:translate-y-0">

        Өзгерту

    </button>

    <button
        class="flex items-center ml-8 px-14 py-3 text-white bg-blue-600 rounded-2xl
             text-white font-semibold focus:outline-none
             transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Тексеріп жіберу

    </button>

</div>
