<div class="pt-16">

    <h2 class="text-2xl text-gray-400 font-semibold">Тапсырма</h2>

<!--    <h2 class="text-2xl text-gray-400 font-semibold">бағаланды: 100</h2>-->
<!--    <h2 class="text-2xl text-gray-400 font-semibold">Тапсырма тексеріліп жатыр</h2>-->

    <h1 class="mt-6 text-4xl font-semibold">Бизнес-тапсырма жасау</h1>

    <p class="mt-16 text-xl">
        Web дизайн - бұл интернет-ресурс, оған үшінші тарап пайдаланушылары кірген кезде иесі коммерциялық пайда алады. Дұрыс дизайн, мәтіндердің семантикалық жүктемесі компаниялардың/интернет-дүкендердің әлеуетті клиенттерін тартады. Сату сайтының жарқын мысалы-LandingPage.
    </p>

    <form
            x-data="{openQuestion: false}"
            action=""
            class="mt-16"
    >

      <?php include "adds/add-processing-form.php"; ?>

    </form>

</div>
