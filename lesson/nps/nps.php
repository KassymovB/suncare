<div
        x-show="openNps"
        x-trasnition:enter="transition ease-liner duration-700"
        x-trasnition:leave="transition ease-liner duration-700"
        class="fixed inset-0 z-10"
>

    <div
            x-show="openNps"
            @click.prevent="openNps = !openNps"
            x-cloak=""
            x-transition:enter="transition ease-liner duration-400"
            x-transition:enter-start="opacity-0"
            x-transition:enter-end="opacity-100"
            x-transition:leave="transition ease-liner duration-700"
            x-transition:leave-start="opacity-100"
            x-transition:leave-end="opacity-0"
            class="absolute inset-0 bg-black-light bg-opacity-50"
    >

    </div>

    <div
            x-show="openNps"

            x-transition:enter="transition ease-liner duration-700"
            x-transition:enter-start="opacity-0 transform translate-y-96"
            x-transition:enter-end="opacity-1 transform translate-y-0"
            x-transition:leave="transition ease-liner duration-700"
            x-transition:leave-start="opacity-1 transform translate-y-0"
            x-transition:leave-end="opacity-0 transform translate-y-96"

            class="absolute bottom-0 inset-x-0 flex flex-col justify-center items-center pt-14 py-20 bg-white"
    >

        <button
                @click="openNps = !openNps"
                class="absolute top-5 right-5 text-gray-400 text-2xl focus:outline-none">
            &times;
        </button>

        <h3 class="font-semibold">
            Сабақ қалай болды?
        </h3>

        <div class="flex items-center">
            <span class="inline-block mr-7 text-gray-400 font-bold">  Жаман</span>

            <form>
                <div class="stars-rating">
                    <div class="stars-rating__stars">

                        <input type="radio" class="stars-rating__star" id="1" name="star" value="5">
                        <label for="1" class="stars-rating__label-star"></label>
                        <input type="radio" class="stars-rating__star"  id="2" name="star" value="4">
                        <label for="2" class="stars-rating__label-star"></label>
                        <input type="radio" class="stars-rating__star" id="3" name="star" value="3">
                        <label for="3" class="stars-rating__label-star"></label>
                        <input type="radio" class="stars-rating__star" id="4" name="star" value="2">
                        <label for="4" class="stars-rating__label-star"></label>
                        <input type="radio" class="stars-rating__star" id="5" name="star" value="1">
                        <label for="5" class="stars-rating__label-star"></label>
                    </div>

                </div>
            </form>

            <span class="inline-block ml-7 text-gray-400 font-bold">  Өте жақсы</span>
        </div>
    </div>
</div>
