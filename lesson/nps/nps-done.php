<div
    x-show="openNps"
    x-cloak=""
    x-trasnition:enter="transition ease-liner duration-700"
    x-trasnition:leave="transition ease-liner duration-700"
    class="fixed inset-0 z-10"
>

    <div
        x-show="openNps"
        @click.prevent="openNps = !openNps"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-400"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        class="absolute inset-0 bg-black-light bg-opacity-50"
    >

    </div>

    <div
        x-show="openNps"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-700"
        x-transition:enter-start="opacity-0 transform translate-y-96"
        x-transition:enter-end="opacity-1 transform translate-y-0"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-1 transform translate-y-0"
        x-transition:leave-end="opacity-0 transform translate-y-96"

        class="flex items-center justify-center absolute bottom-0 inset-x-0 py-28 bg-white"
    >

        <button
            @click="openNps = !openNps"
            class="absolute top-5 right-5 text-gray-400 text-2xl focus:outline-none">
            &times;
        </button>


        <div class="">
            <div class="mx-auto w-14 h-14">
                <img src="/img/done.png" alt="done" class="w-full h-full">
            </div>

            <div class="mt-12 text-4xl font-semibold">
                Пікіріңе рахмет!
            </div>
        </div>

    </div>
</div>
