<div
    x-show="openNps"
    x-cloak=""
    x-trasnition:enter="transition ease-liner duration-700"
    x-trasnition:leave="transition ease-liner duration-700"
    class="fixed inset-0 z-10"
>

    <div
        x-show="openNps"
        @click.prevent="openNps = !openNps"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-400"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        class="absolute inset-0 bg-black-light bg-opacity-50"
    >

    </div>

    <div
        x-show="openNps"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-700"
        x-transition:enter-start="opacity-0 transform translate-y-96"
        x-transition:enter-end="opacity-1 transform translate-y-0"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-1 transform translate-y-0"
        x-transition:leave-end="opacity-0 transform translate-y-96"

        class="flex items-center justify-center absolute bottom-0 inset-x-0 py-6 bg-white"
    >

        <button
            @click="openNps = !openNps"
            class="absolute top-5 right-5 text-gray-400 text-2xl focus:outline-none">
            &times;
        </button>


        <button  class="flex items-center justify-center w-9 h-9 transform rotate-180
        bg-gray-200 rounded-full focus:outline-none">
            <svg class="w-4 h-4 fill-current text-white">
                <use href="#chevron-right"></use>
            </svg>
        </button>



        <div class="mx-14 max-w-lg">

            <ul class="flex items-center pb-3 border-b-2 border-gray-200">

                <li class="relative text-blue-700 font-semibold">
                    Платформа
                    <div class="absolute -bottom-3.5 inset-x-0 w-full bg-blue-700 h-0.5"></div>
                </li>

                <li class="ml-10 text-gray-500 font-semibold">Контент</li>

                <li class="ml-10 text-gray-500 font-semibold">Контент</li>

                <li class="ml-10 text-gray-500 font-semibold">Спикер</li>

                <li class="ml-10 text-gray-500 font-semibold">Басқа</li>

            </ul>


            <h3 class="mt-14 font-semibold">
                Қандай қиындықтар туындады?
            </h3>


            <form class="mt-7">

                <textarea name="text" cols="30" rows="10"
                          class="w-full h-36 p-6 resize-none rounded-xl border-2 border-gray-100
                           focus:border-blue-600 focus:outline-none font-semibold"
                >

                </textarea>

                <?php include 'active-btn.php';?>


            </form>


        </div>

    </div>
</div>
