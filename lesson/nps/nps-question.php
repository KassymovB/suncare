<div
    x-show="openNps"
    x-cloak=""
    x-trasnition:enter="transition ease-liner duration-700"
    x-trasnition:leave="transition ease-liner duration-700"
    class="fixed inset-0 z-10"
>

    <div
        x-show="openNps"
        @click.prevent="openNps = !openNps"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-400"
        x-transition:enter-start="opacity-0"
        x-transition:enter-end="opacity-100"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-100"
        x-transition:leave-end="opacity-0"
        class="absolute inset-0 bg-black-light bg-opacity-50"
    >

    </div>

    <div
        x-show="openNps"
        x-cloak=""
        x-transition:enter="transition ease-liner duration-700"
        x-transition:enter-start="opacity-0 transform translate-y-96"
        x-transition:enter-end="opacity-1 transform translate-y-0"
        x-transition:leave="transition ease-liner duration-700"
        x-transition:leave-start="opacity-1 transform translate-y-0"
        x-transition:leave-end="opacity-0 transform translate-y-96"

        class="flex items-center justify-center absolute bottom-0 inset-x-0 py-6 bg-white"
    >

        <button
            @click="openNps = !openNps"
            class="absolute top-5 right-5 text-gray-400 text-2xl focus:outline-none">
            &times;
        </button>


        <button
                @click="openNps = !openNps"
                class="absolute top-5 left-5 text-gray-400 text-2xl focus:outline-none">

            <svg class="w-4 h-4 fill-current">
                <use href="#arrow-left"></use>
            </svg>
        </button>


        <button  class="flex items-center justify-center w-9 h-9 transform rotate-180
        bg-gray-200 rounded-full focus:outline-none">

            <svg class="w-4 h-4 fill-current text-white">
                <use href="#chevron-right"></use>
            </svg>
        </button>



        <div class="mx-14" style="width: 512px;">

            <ul class="flex items-center justify-between pb-3 border-b-2 border-gray-200">

                <li class="relative text-blue-700 font-semibold">
                    Платформа
                    <div class="absolute -bottom-3.5 inset-x-0 w-full bg-blue-700 h-0.5"></div>
                </li>

                <li class="text-gray-500 font-semibold">Контент</li>

                <li class="text-gray-500 font-semibold">Спикер</li>

                <li class="text-gray-500 font-semibold">Басқа</li>

            </ul>


            <h3 class="mt-14 font-semibold">
                Қандай қиындықтар туындады?
            </h3>


            <form class="mt-7">

                <div class="flex items-center">
                    <input id="1-variant" type="checkbox" class="green-checkbox">
                    <label for="1-variant" class="font-semibold">Баяу істейді</label>
                </div>

                <div class="mt-4 flex items-center">
                    <input id="2-variant" type="checkbox" class="green-checkbox">
                    <label for="2-variant" class="font-semibold">Баяу істейді </label>
                </div>

                <div class="mt-4 flex items-center">
                    <input id="3-variant" type="checkbox" class="green-checkbox">
                    <label for="3-variant" class="font-semibold">Баяу істейді</label>
                </div>

                <div class="mt-4 flex items-center">
                    <input id="4-variant" type="checkbox" class="green-checkbox">
                    <label for="4-variant" class="font-semibold">Баяу істейді</label>
                </div>

<!--                контент-->
<!---->
<!--                <div class="flex items-center">-->
<!--                    <input id="1-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="1-variant" class="font-semibold">Тым ауыр</label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="2-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="2-variant" class="font-semibold">Тым жеңіл </label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="3-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="3-variant" class="font-semibold">Өзексіз тақырыптар</label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="4-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="4-variant" class="font-semibold">Түсініксіз</label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="4-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="4-variant" class="font-semibold">Тақырып толық ашылмады</label>-->
<!--                </div>-->

                <!--   спикер -->
<!---->
<!--                <div class="flex items-center">-->
<!--                    <input id="1-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="1-variant" class="font-semibold">Қызықсыз</label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="2-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="2-variant" class="font-semibold">Түсініксіз </label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="3-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="3-variant" class="font-semibold">Пайдасыз тақырыптар</label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="4-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="4-variant" class="font-semibold">Жүйесіз түсіндіру</label>-->
<!--                </div>-->
<!---->
<!--                <div class="mt-4 flex items-center">-->
<!--                    <input id="4-variant" type="checkbox" class="green-checkbox">-->
<!--                    <label for="4-variant" class="font-semibold">Өте ұзақ</label>-->
<!--                </div>-->

                <?php include 'disabled-btn.php';?>

            </form>


        </div>


        <button  class="flex items-center justify-center w-9 h-9 bg-gray-200 rounded-full focus:outline-none">
            <svg class="w-4 h-4 fill-current text-white">
                <use href="#chevron-right"></use>
            </svg>
        </button>


    </div>
</div>
