
<div>
    <div class="relative h-20">
        <input
                required
                id="name"
                type="tel"
                name="tel"
                class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-red-400 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
        >

        <!--    custom
            <label
                for="name"
                class="login-label text-gray-400 font-semibold">
                Телефон нөмірі
            </label>
            -->
        <label
                for="name"
                class="login-label text-red-400 font-semibold cursor-text">
            Телефон нөмірі
        </label>


    </div>

    <div class="mt-2 text-sm text-red-400 font-semibold">
        Нөмір енгізілу керек
    </div>
</div>


<div class="mt-12">


    <div class="relative h-20">
        <input
                required
                id="password"
                type="password"
                name="password"
                class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-red-400 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
        >

        <!--    custom
            <label
                for="name"
                class="login-label text-gray-400 font-semibold">
                Телефон нөмірі
            </label>
            -->
        <label
                for="password"
                class="login-label text-red-400 font-semibold cursor-text">
            Құпиясөз
        </label>

        <button type="button" class="absolute top-2/4 right-6 transform -translate-y-2/4 focus:outline-none">

            <svg class="w-5 h-5 fill-current text-gray-500">
                <use href="#eyes"></use>
            </svg>

            <!--    active-btn---
                <svg class="w-5 h-5 fill-current text-blue-600">
                    <use href="#eyes"></use>
                </svg>-->

        </button>


    </div>

    <div class="mt-2 text-sm text-red-400 font-semibold">
        Нөмір енгізілу керек
    </div>
</div>
