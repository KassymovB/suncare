<div class="w-full">

    <div class="relative mx-auto max-w-lg h-full">

        <?php include "alert.php"?>

        <div class="flex flex-col justify-center h-full">

            <h1 class="mt-12 text-4xl font-semibold">
                SunCare cервисіне
                қош келдің!
            </h1>

            <form class="mt-8">

                <?php include "inputs.php";?>

                <a href="#" class="block mt-8 text-blue-600 text-lg font-semibold text-right hover:underline">
                    Құпиясөз есімде емес
                </a>

                <button
                        @click.prevent="openAlert = !openAlert"

                        class="flex items-center justify-center mt-8 px-20 py-4 w-full bg-blue-600 rounded-2xl
                     text-white font-semibold focus:outline-none
                     transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
                >
                    Кіру

                </button>

                <div class="mt-8 font-semibold text-center">
                    Аккаунт әлі жоқ па?

                    <a href="#" class="inline-block ml-2 text-blue-600 hover:underline">
                        Тіркелу
                    </a>
                </div>

            </form>

        </div>

    </div>

</div>
