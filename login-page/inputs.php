<div class="relative h-20">
    <input
            required
            id="name"
            type="tel"
            name="tel"
            class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-gray-100 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
    >

    <label
            for="name"
            class="login-label text-gray-400 font-semibold">
        Телефон нөмірі
    </label>


<!--
    <label
            for="name"
            class="login-label text-red-400 font-semibold cursor-text">
        Телефон нөмірі
    </label>-->

</div>

<div class="relative mt-12 h-20">

    <?php include "custom-password-btn.php"; ?>

    <input
            required
            id="password"
            type="password"
            name="password"
            class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-gray-100 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
    >


    <label
            for="password"
            class="login-label text-gray-400 font-semibold">
        Құпиясөз
    </label>

    <!-- <label
            for="name"
            class="login-label text-red-400 font-semibold cursor-text">
        Телефон нөмірі
    </label>-->


</div>
