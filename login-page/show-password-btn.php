<button type="button" class="absolute top-2/4 right-6 transform -translate-y-2/4 text-gray-500 focus:text-blue-600 focus:outline-none z-20">

    <svg class="w-5 h-5 fill-current">
        <use href="#show-password"></use>
    </svg>

</button>