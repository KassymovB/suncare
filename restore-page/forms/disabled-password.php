
    <div class="">
        Келесі сипаттарға сай жаңа кұпиясөз құрастыр:
    </div>

    <div class="flex items-center text-green-500">

        <svg class="mr-3 w-4 h-4 fill-current">
            <use href="#check-bold"></use>
        </svg>

        Латын әріптері мен цифрлары
    </div>

    <div class="flex items-center text-gray-500">
        <svg class="mr-3 w-4 h-4 fill-current">
            <use href="#check-bold"></use>
        </svg>

        Кемінде 6 символ
    </div>

    <div class="mt-8">

        <div class="relative h-20">
            <input
                    required
                    id="password"
                    type="password"
                    name="password"
                    class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-red-400 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
            >

            <label
                    for="password"
                    class="login-label text-red-400 font-semibold">
                Жаңа құпиясөзді қайталау
            </label>

            <?php include "./btn/custom-password-btn.php" ?>


        </div>


        <div class="mt-2 text-sm text-red-400 font-semibold">
            Сипаттарға сай емес
        </div>
    </div>

    <div class="mt-4">

        <div class="relative h-20">
            <input
                    required
                    id="password2"
                    type="password"
                    name="password2"
                    class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-red-400 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
            >

            <label
                    for="password2"
                    class="login-label text-red-400 font-semibold">
                Жаңа құпиясөзді қайталау
            </label>

            <?php include "./btn/custom-password-btn.php" ?>


        </div>

        <div class="mt-2 text-sm text-red-400 font-semibold">
            Сәйкес келмейді
        </div>

    </div>

    <button
        @click.prevent="openPassword = !openPassword"
        class="flex items-center justify-center mt-8 px-20 py-4 w-full bg-blue-600 rounded-2xl
                     text-white font-semibold focus:outline-none
                     transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Сақтау

    </button>


