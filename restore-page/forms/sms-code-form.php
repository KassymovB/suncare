

    <div class="">
        +7 708 708 0850
        нөміріне жіберілген SMS кодты енгіз
    </div>

    <div class="mt-8">

        <div class="relative h-20">
            <input
                    required
                    id="text"
                    type="text"
                    name="text"
                    class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-gray-100 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
            >

            <label
                    for="text"
                    class="login-label text-gray-400 font-semibold">
                SMS код
            </label>


            <!--
                <label
                        for="name"
                        class="login-label text-red-400 font-semibold cursor-text">
                    Телефон нөмірі
                </label>-->

        </div>


        <div class="flex items-center justify-between mt-3">

            <div class="ml-6 text-sm font-semibold">
                0:59
            </div>

            <button class="text-sm text-gray-300 font-semibold focus:outline-none">
                Қайта жіберу
            </button>

        </div>

    </div>


    <button
            @click.prevent="openSMS = !openSMS"
        class="flex items-center justify-center mt-16 px-20 py-4 w-full bg-blue-600 rounded-2xl
                     text-white font-semibold focus:outline-none
                     transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Әрі қарай

    </button>


