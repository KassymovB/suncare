
    <div class="">
        Құпиясөзді қалпына келтіру үшін тіркелген телефон нөмірін жазып жібер
    </div>

    <div class="relative mt-14 h-20">
        <input
                required
                id="name"
                type="tel"
                name="tel"
                class="login-input px-6 w-full h-full pt-4 bg-gray-100 border-2 border-gray-100 rounded-2xl font-semibold focus:outline-none focus:border-blue-700"
        >

        <label
                for="name"
                class="login-label text-gray-400 font-semibold">
            Телефон нөмірі
        </label>


        <!--
            <label
                    for="name"
                    class="login-label text-red-400 font-semibold cursor-text">
                Телефон нөмірі
            </label>-->

    </div>

    <button

            class="flex items-center justify-center mt-8 px-20 py-4 w-full bg-blue-600 rounded-2xl
                     text-white font-semibold focus:outline-none
                     transition-all ease-linear duration-150 transform hover:-translate-y-2 hover:bg-blue-500 hover:shadow-2xl focus:translate-y-0"
    >
        Әрі қарай

    </button>

