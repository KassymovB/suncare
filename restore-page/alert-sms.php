
<div
    x-show="openSMS"
    x-cloak=""
    x-transition:enter="transition-all ease-liner duration-700"
    x-transition:enter-start="opacity-0 transform -translate-y-16"
    x-transition:enter-end="opacity-1 transform translate-y-0"

    x-transition:leave="transition-all ease-liner duration-700"
    x-transition:leave-start="opacity-1 transform translate-y-0"
    x-transition:leave-end="opacity-0 transform -translate-y-16"

    class="absolute top-10 inset-x-0 flex justify-center">

    <div class="flex items-center justify-center py-4 px-7 w-full rounded-2xl border-2 border-red-500 text-sm text-red-500 font-semibold">

        <svg class="flex-shrink-0 mr-3 w-5 h-5 fill-current">
            <use href="#danger"></use>
        </svg>

        Серверда қателік кетті. Кейінірек жазып көр


        <!--SMS код сәйкес емес-->
    </div>
</div>