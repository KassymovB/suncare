<div class="w-full">

    <div class="relative mx-auto h-full max-w-lg ">

        <?php include "alert-sms.php" ?>

        <div class="flex flex-col justify-center h-full">
            <div>

                <a href="#" class="inline-flex items-center text-gray-500 hover:text-gray-400 font-semibold">

                    <svg class="mr-2 w-3 h-3 fill-current transform rotate-180">
                        <use href="#chevron-right"></use>
                    </svg>

                    Қайту
                </a>
            </div>

            <h1 class="mt-4 text-4xl font-semibold">
                Құпиясөз есімде емес
            </h1>

            <form class="mt-4">

                <?php include "forms/sms-code-form.php" ?>

            </form>
        </div>

    </div>
</div>
