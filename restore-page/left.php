<!--

SMS-block:{

 <img src="/img/sms-code.png" alt="Logo">
text: ID
SMS арқылы
расталады
 }

password-block:{

 <img src="/img/password-check.png" alt="Logo">
text:Болашақта осы
құпиясөз қолданылатын болады

 }

-->


<div class="flex-shrink-0 p-16 pr-24 w-5/12 bg-gradient-to-tl from-violet-600 to-purple-400">

    <div class="">
        <img src="/img/logo_white.png" alt="Logo">
    </div>

    <div class="flex flex-col items-center mt-40">

        <div class="w-52 hoverflow-hidden">
            <img src="/img/lock.png" alt="Login img" class="w-full h-full">
        </div>

        <h1 class="mt-16 text-3xl text-white font-semibold text-center">
            Жеке аккаунт
            телефон нөмірімен ашылады
        </h1>

        <div class="flex items-center mt-10">

            <div class="flex items-center justify-center w-5 h-5 rounded-full bg-white text-sm font-semibold text-violet-600 opacity-50">
                1
            </div>

            <div class="mx-0.5 w-5 h-0.5 bg-white opacity-50"></div>

            <div class="flex items-center justify-center w-5 h-5 rounded-full bg-white text-sm font-semibold text-violet-600">
                2
            </div>

            <div class="mx-0.5 w-5 h-0.5 bg-white opacity-50"></div>

            <div class="flex items-center justify-center w-5 h-5 rounded-full bg-white text-sm font-semibold text-violet-600 opacity-50">
                3
            </div>

        </div>
    </div>

</div>

