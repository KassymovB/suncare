const colors = require('tailwindcss/colors')

module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        current: 'currentColor',
        red: colors.red,
        rose: colors.rose,
        emerald: colors.emerald,
        purple: colors.purple,
        violet: colors.violet,
        lightblue: colors.lightBlue,
        pink: colors.pink,
        orange: colors.orange,

        black: {
          light: '#3D3D3D',
          dark: '#1C1C1C'
        },

      }
    },
  },
  variants: {
    extend: {
      scale: ['group-hover'],
      textColor: ['group-focus'],
    },
  },
  plugins: [],
}
